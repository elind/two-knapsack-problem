import random
from collections import Counter

stuffdict = {'nb_13': (2451, 40),
             'nb_14': (2978, 35),
             'nb_o': (3625, 80),
             'mt_b': (717, 30),
             'mt_0': (988, 60),
             'mt_hd': (1220, 65),
             'tb_k': (1405, 40),
             'tb_g': (1455, 40),
             'to_k': (1690, 45),
             'to_g': (1980, 68)
             }

stuffcount = {'nb_13': 205,
              'nb_14': 420,
              'nb_o': 450,
              'mt_b': 60,
              'mt_0': 157,
              'mt_hd': 220,
              'tb_k': 620,
              'tb_g': 250,
              'to_k': 540,
              'to_g': 370,
              }

max_load_t1 = 1100000 - 85700
max_load_t2 = 1100000 - 72400

stuff_by_eff = {}
stuff_by_weight = {}
for item in stuffdict:
    stuff_by_eff[item] = stuffdict[item][1] / stuffdict[item][0]
    stuff_by_weight[item] = stuffdict[item][0]
stuff_by_eff = {k: v for k, v in sorted(stuff_by_eff.items(), key=lambda x: x[1], reverse=True)}
stuff_by_eff_keys = [*stuff_by_eff]
stuff_by_weight = {k: v for k, v in sorted(stuff_by_weight.items(), key=lambda x: x[1])}
min_weight = min(list(stuff_by_weight.values()))

title = ''
for item in stuffcount:
    title += ' {0:7}'.format(item)


def upper_bound_for_solution():
    result = 0
    free_load = max_load_t1 + max_load_t2
    for item in stuff_by_eff:
        w = stuffdict[item][0]
        v = stuffdict[item][1]
        count = stuffcount[item]
        taken_count = min((int(free_load / w + 0.5), count))
        result += v * taken_count
        free_load -= taken_count * w
    return result


def available_items(solution):
    counter = Counter(stuffcount)
    counter.subtract(solution.stuffcount_t1)
    counter.subtract(solution.stuffcount_t2)
    return counter


def fill_to_full(solution, balance=0.5):
    """ Greedy solution for two transporters. The most valuable items will be added."""

    available = available_items(solution)
    for item in stuff_by_eff:
        w1, w2 = solution.get_weight()
        free_t1, free_t2 = max_load_t1 - w1, max_load_t2 - w2
        if max_load_t1 - w1 < min_weight and max_load_t2 - w2 < min_weight:
            break
        available_eff_items = available[item]
        w = stuff_by_weight[item]
        place_for_items_t1 = free_t1 // w
        place_for_items_t2 = free_t2 // w
        t1_count = int(balance * available_eff_items)
        t2_count = int((1 - balance) * available_eff_items)
        t1_load = min(place_for_items_t1, t1_count)
        t2_load = min(place_for_items_t2, t2_count)
        solution.stuffcount_t1[item] += t1_load
        solution.stuffcount_t2[item] += t2_load
        available_eff_items -= t1_load + t2_load
        if available_eff_items > 0:
            w1, w2 = solution.get_weight()
            free_t1, free_t2 = max_load_t1 - w1, max_load_t2 - w2
            odin = min(free_t1 // w, available_eff_items)
            if free_t1 > w:
                solution.stuffcount_t1[item] += odin
            available_eff_items -= odin
            if free_t2 > w:
                solution.stuffcount_t2[item] += min(free_t2 // w, available_eff_items)

    return solution


def reduce_to_max_count(solution):
    """ This method remove items (genes) that exceed the limit after crossbreeding"""
    counter = available_items(solution)
    for item in counter:
        count = counter[item]
        if count < 0:
            count = -1 * count
            to_unload = count // 2
            t1_count = solution.stuffcount_t1[item]
            t2_count = solution.stuffcount_t2[item]
            real_unload_t1 = min(to_unload, t1_count)
            real_unload_t2 = min(to_unload, t2_count)
            solution.stuffcount_t1[item] -= real_unload_t1
            solution.stuffcount_t2[item] -= real_unload_t2
            real_unload = real_unload_t1 + real_unload_t2
            not_unloaded = count - real_unload
            if not_unloaded > 0:
                if t1_count - real_unload_t1 > 0:
                    solution.stuffcount_t1[item] -= not_unloaded
                if t2_count - real_unload_t2 > 0:
                    solution.stuffcount_t2[item] -= not_unloaded
    fill_to_full(solution, random.uniform(0, 1))
    return solution
