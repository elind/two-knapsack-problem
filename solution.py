# This class represent a solution for backpack problem
from data import available_items, max_load_t1, max_load_t2


class Solution:
    def __init__(self, stuffdict, stuffcount_t1=None, stuffcount_t2=None):
        if stuffcount_t1 is None:
            stuffcount_t1 = {}
            for item in stuffdict:
                stuffcount_t1[item] = 0
        if stuffcount_t2 is None:
            stuffcount_t2 = {}
            for item in stuffdict:
                stuffcount_t2[item] = 0
        self.stuffdict = stuffdict
        self.stuffcount_t1 = stuffcount_t1
        self.stuffcount_t2 = stuffcount_t2

    def get_fitness(self):
        fitness = 0
        for item in self.stuffdict:
            v = self.stuffdict[item][1]
            c1 = self.stuffcount_t1[item]
            c2 = self.stuffcount_t2[item]
            fitness += (c1 + c2) * v
        return fitness

    def get_weight(self):
        weight_t1 = 0
        weight_t2 = 0
        for item in self.stuffdict:
            w = self.stuffdict[item][0]
            c1 = self.stuffcount_t1[item]
            c2 = self.stuffcount_t2[item]
            weight_t1 += c1 * w
            weight_t2 += c2 * w
        return weight_t1, weight_t2

    def __str__(self):
        formatstr = ' {0:7}'
        line1 = ''
        line2 = ''
        for item in self.stuffcount_t1:
            line1 += formatstr.format(str(self.stuffcount_t1[item]))
        for item in self.stuffcount_t1:
            line2 += formatstr.format(str(self.stuffcount_t2[item]))

        return line1 + '\n' + line2 + '\n'

    def __eq__(self, other):
        if not isinstance(other, Solution):
            return NotImplemented

        return self.stuffcount_t1 == other.stuffcount_t1 and self.stuffcount_t2 == other.stuffcount_t2
