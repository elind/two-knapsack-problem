import copy
import random as rd

import matplotlib.pyplot as plt
import numpy as np

from data import *
from solution import Solution


def generate_greedy_solutions(n):
    generated = 0
    result = []
    while generated < n:
        solution = Solution(stuffdict)
        fill_to_full(solution, balance=generated / n)
        result.append(solution)
        generated += 1
    rd.shuffle(result)
    return result


def selection(fitness, num_parents, population):
    fitness = list(fitness)
    parents = []
    for i in range(num_parents):
        max_value = max(fitness)
        max_idx = fitness.index(max_value)
        parents.append(population[max_idx])
        fitness[max_idx] = 0
    return parents


def crossover(parents, num_offsprings):
    offsprings = []
    parents_count = len(parents) - 1
    for i in range(0, num_offsprings):
        parent1_idx = rd.randint(0, parents_count)
        parent2_idx = rd.randint(0, parents_count - 1)
        if parent2_idx == parent1_idx:
            parent2_idx = parents_count
        child1, child2 = crossbreeding(parents[parent1_idx], parents[parent2_idx])
        if child1 not in parents:
            offsprings.append(child1)
        if child2 not in parents:
            offsprings.append(child2)
    return offsprings


def crossbreeding(solution1, solution2):
    chromosome11 = copy.deepcopy(solution1.stuffcount_t1)
    chromosome12 = copy.deepcopy(solution1.stuffcount_t2)
    chromosome21 = copy.deepcopy(solution2.stuffcount_t1)
    chromosome22 = copy.deepcopy(solution2.stuffcount_t2)
    offspring1 = Solution(stuffdict, chromosome11, chromosome22)
    offspring2 = Solution(stuffdict, chromosome21, chromosome12)
    reduce_to_max_count(offspring1)
    reduce_to_max_count(offspring2)
    return offspring1, offspring2


def mutation(offsprings, mutation_rate=0.25, max_gene_mutation=5):
    offsprings_count = len(offsprings)
    for i in range(offsprings_count):
        random_value = rd.random()
        if random_value > mutation_rate:
            continue
        mutant = offsprings[i]
        w1, w2 = mutant.get_weight()
        if bool(rd.getrandbits(1)):
            chromosome = mutant.stuffcount_t1
            wt = w1
            max_load = max_load_t1
        else:
            chromosome = mutant.stuffcount_t2
            wt = w2
            max_load = max_load_t2

        genes_to_lost = rd.randint(1, max_gene_mutation)
        available_genes = [x for x, y in chromosome.items() if y >= genes_to_lost]

        old_item = rd.choice(available_genes)
        w = stuffdict[old_item][0]
        free_load = max_load + genes_to_lost * w - wt
        chromosome[old_item] -= genes_to_lost

        cnt = available_items(mutant)
        possible_genes_to_obtain = list(cnt)
        possible_genes_to_obtain.remove(old_item)
        new_item = rd.choice(possible_genes_to_obtain)
        nw = stuffdict[new_item][0]
        chromosome[new_item] += min(free_load // nw, cnt[new_item])
        fill_to_full(mutant)
    return offsprings


def optimize(population, num_generations, mutation_rate=0.25, max_gene_mutation=5):
    max_sol = 0
    print(title)

    result, fitness_history = [], []
    num_parents = len(population)
    for i in range(num_generations):
        fitness = list(map(lambda x: x.get_fitness(), population))
        fitness_history.append(fitness)
        parents = selection(fitness, num_parents, population)

        # Print block of the best solution
        cur_max = parents[0].get_fitness()
        if cur_max > max_sol:
            max_sol, result = cur_max, parents[0]
            print(result, "A better solution has been found:", cur_max)
        # End of block

        offsprings = crossover(parents, num_parents // 2)
        mutants = mutation(offsprings, mutation_rate, max_gene_mutation)
        population[0:num_parents] = parents
        population[num_parents:] = mutants
        while len(population) < num_parents:
            foreigner = Solution(stuffdict)
            fill_to_full(foreigner, balance=rd.uniform(0, 1))
            result.append(foreigner)

    return result, fitness_history


def genetic(solutions_per_pop=8, num_generations=1000, mutation_rate=0.25, max_gene_mutation=5, plot=True):
    print('Genetic algorithm starts')
    print('The upper bound for the solution: ', upper_bound_for_solution())
    initial_population = generate_greedy_solutions(solutions_per_pop)
    result, fitness_history = optimize(initial_population, num_generations, mutation_rate, max_gene_mutation)
    fitness_history_mean = [np.mean(fitness) for fitness in fitness_history]
    fitness_history_max = [np.max(fitness) for fitness in fitness_history]
    max_value = max(fitness_history_max)
    print('\nThe best found solution:\n', title, '\n', result, 'with total value: ', max_value)
    if plot:
        plt.plot(list(range(num_generations)), fitness_history_mean, label='Mean Fitness')
        plt.plot(list(range(num_generations)), fitness_history_max, label='Max Fitness')
        plt.legend()
        plt.title('Fitness through the generations')
        plt.xlabel('Generations')
        plt.ylabel('Fitness')
    plt.show()
    return result


if __name__ == '__main__':
    result = genetic(solutions_per_pop=64, num_generations=500, mutation_rate=0.95, max_gene_mutation=7, plot=True)
