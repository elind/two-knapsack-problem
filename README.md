# The challenge

Users in Bonn need the various notebooks, phones and tablets with varying degrees of urgency. A utility value has therefore been determined for each type of device. The goal is to achieve the highest possible cumulative benefit with only one delivery trip.

There are 2 vans available for the trip to the Bonn. The challenge is to develop an algorithm, which balance the load on two transporters optimally. The loading capacity of the transporters as well as the different weights of the hardware and the demand at the Bonn location must be taken into account. The result should be an optimal loading list.

# The data

Hardware | Required number of units in Bonn | Weight of one unit | Value of one unit 
--- 						|	--- 	|	---		|	--- 
Notebook 13					|	205 	| 	2451	|	40
Notebook 14					|	420 	| 	2978	|	35
Notebook outdoor			|	450 	| 	2451	|	80
Mobiletelefon office		|	60		|	717		|	30
Mobiletelefon outdoor		|	157		|	988		|	60
Mobiletelefon heavy duty	|	220		|	1220	|	65
Tablet office small			|	620		|	1405	|	40
Tablet office big			|	250		|	1455	|	40
Tablet outdoor small		|	540		|	1690	|	45
Tablet outdoor big			|	370		|	1980	|	68

Maximum load capacity per transporter: 1 100kg (1 100 000g)
Driver weights: 72,4 kg (72 400g) and 85,7 kg (85 700g)

Those data are saved in two dictionaries in the data.py file: stuffdict and stuffcount.

# Usage
To run the algorithm, just execute the main method in genetic_ng.py.

# The solution

To solve the problem the following strategy is chosen:
1. A greedy algorithm is used to find a suboptimal solution.
2. A local search based on the modified genetic algorithm is applied.

The best solution obtained by the algorithm:

Hardware | Total in T1 | Total in T2 | 
--- 						|	--- 	|	---		|	
Notebook 13					|	- 		| 	-   	|	
Notebook 14					|	- 		| 	-     	|	
Notebook outdoor			|	- 		| 	-   	|
Mobiletelefon office		|	37		|	23		|
Mobiletelefon outdoor		|	96		|	61		|
Mobiletelefon heavy duty	|	132		|	88	    |
Tablet office small			|	231		|	364	    |
Tablet office big			|	-		|	-	    |
Tablet outdoor small		|	2		|	2   	|
Tablet outdoor big			|	204		|	166 	|
--- 						|	     	|			| 
Total weight				|	101472g|	1027599g|
Unused payload				|	28g     |	1g      |

with total value of all items equals to 74660. A little bit of luck needed =)

# Requirements

Python version 3.7 or higher is required.
Numpy and matplotlib if you want some graphics.
